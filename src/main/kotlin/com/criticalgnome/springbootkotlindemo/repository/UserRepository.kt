package com.criticalgnome.springbootkotlindemo.repository

import com.criticalgnome.springbootkotlindemo.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: JpaRepository<User, Long>