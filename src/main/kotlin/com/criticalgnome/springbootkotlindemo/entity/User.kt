package com.criticalgnome.springbootkotlindemo.entity

import javax.persistence.*

@Entity
data class User(
        @Id @GeneratedValue val id: Long,
        val firstName: String,
        val lastName: String
)