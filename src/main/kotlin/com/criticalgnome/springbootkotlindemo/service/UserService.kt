package com.criticalgnome.springbootkotlindemo.service

import com.criticalgnome.springbootkotlindemo.entity.User
import com.criticalgnome.springbootkotlindemo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService(@Autowired private val userRepository: UserRepository): BaseService<User>(userRepository)