package com.criticalgnome.springbootkotlindemo.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

open class BaseService<T>(private val repository: JpaRepository<T, Long>) {
    fun getOne(id: Long): T = repository.getOne(id)
    fun getAll(pageable: Pageable): Page<T> = repository.findAll(pageable)
    fun save(t: T): T = repository.save(t)
    fun delete(id: Long) = repository.deleteById(id)
    fun count(): Long = repository.count()
}