package com.criticalgnome.springbootkotlindemo.controller

import com.criticalgnome.springbootkotlindemo.entity.User
import com.criticalgnome.springbootkotlindemo.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SuppressWarnings("unused")
@RestController
@RequestMapping("users")
class UserController(@Autowired private val userService: UserService): BaseController<User>(userService)