package com.criticalgnome.springbootkotlindemo.controller

import com.criticalgnome.springbootkotlindemo.service.BaseService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*

@SuppressWarnings("unused")
open class BaseController<T>(private val service: BaseService<T>) {
    @GetMapping("{id}")    fun getOne(@PathVariable id: Long): T = service.getOne(id)
    @GetMapping                   fun getAll(pageable: Pageable): Page<T> = service.getAll(pageable)
    @PostMapping                  fun create(@RequestBody t: T): T = service.save(t)
    @PutMapping                   fun update(@RequestBody t: T): T = service.save(t)
    @DeleteMapping("{id}") fun delete(@PathVariable id: Long) = service.delete(id)
    @GetMapping("count")   fun count(): Long = service.count()
}